import glob
import codecs
import nltk

from bs4 import BeautifulSoup

def text_generator():
    for path in glob.glob('../extracted/**/wiki*'):
        with codecs.open(path, 'r', 'utf-8') as f_text:
            bs = BeautifulSoup(f_text.read(), 'lxml')
            for doc in bs.find_all('doc'):
                yield doc.get_text()


if __name__ == "__main__":
    text = text_generator()
    doc = text.next()

    fdist = nltk.FreqDist(ch.lower() for ch in doc)

    print doc
    print fdist.most_common(5)
