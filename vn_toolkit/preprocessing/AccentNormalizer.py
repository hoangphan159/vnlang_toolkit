# -*- coding: utf-8 -*-

import re
import codecs
import os


class AccentNormalizer(object):
    """
    An accent normalizer for Vietnamese string, since the lexicon contains only the later form.
    """

    def __init__(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        self.rules = dict()

        with codecs.open(current_dir + '/rules.txt', 'r', 'utf-8') as normalized_rules:
            for line in normalized_rules.readlines():
                word, normalized_word = line.split()

                self.rules[word] = normalized_word

    def normalize(self, text):
        for (word, normalized_word) in self.rules.iteritems():
            text = re.sub(u'' + word, u'' + normalized_word, text)

        return text
