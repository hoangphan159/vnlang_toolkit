# -*- coding: utf-8 -*-

import re


class LexerNormalizer(object):
    """
    convert special regex pattern to corresponding entity
    """

    def __init__(self):
        self.rules = [
            ('[+-]?\d+(?:\.\d+)?', '##NUMBER'),
            ('[\w.-]+@[\w.-]+', ' ##EMAIL '),
            ('http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', ' ##URL ')
        ]

    def normalize(self, text):
        for pattern, entity in self.rules:
            text = re.sub(pattern, entity, text)

        return text

if __name__ == '__main__':
    lexerNormalizer = LexerNormalizer()
    print lexerNormalizer.normalize(
        """Sample text for testing:
abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ
0123456789 _+-.,!@#$%^&*();\/|<>"'
12345 -98.7 3.141 .6180 9,000 +42
555.123.4567	+1-(800)-555-2468
foo@demo.net	bar.ba@test.co.uk
www.demo.com	http://foo.co.uk/
http://regexr.com/foo.html?q=bar
https://mediatemple.net"
""")