from lxml.etree import iterparse

from nltk.tokenize import sent_tokenize

import codecs


def extract_abstract():

    with codecs.open('../data/abstract/text.txt', 'w', 'utf-8') as abstract:
        # get an iterable
        context = iterparse('../data/viwiki-20160601-abstract.xml', events=("start", "end"))

        # turn it into an iterator
        context = iter(context)

        # get the root element
        event, root = context.next()

        for event, elem in context:
            if event == "end" and elem.tag == "abstract":
                if elem.text:
                    abstract.write(elem.text[elem.text.rfind('|') + 1:] + "\n")
            root.clear()


def sentences_tokenize():
    with codecs.open('../data/abstract/text.txt', 'r', 'utf-8') as abstract:
        with codecs.open('../data/abstract/sentences.copus', 'w', 'utf-8') as sen_files:
            for line in abstract.readlines():
                sentences = sent_tokenize(line)
                sen_files.write("\n".join(sentences))


if __name__ == '__main__':
    # extract_abstract()
    sentences_tokenize()