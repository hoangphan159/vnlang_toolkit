import codecs
import os
import re
import pickle
import multiprocessing
import logging

from tqdm import tqdm

from preprocessing.AccentNormalizer import AccentNormalizer
from preprocessing.LexerNormalizer import LexerNormalizer
from nltk.probability import FreqDist
from nltk.util import ngrams
from sklearn.externals import joblib
from collections import Counter

from gensim.models import Word2Vec
from utils.logging_utils import setup_logging


import heapq as _heapq

DICT_DIR = '../data/dictionary/'
NUM_WORKERS = multiprocessing.cpu_count()

setup_logging()


def corpus_2_sentences(corpus_path):
    accent_normalizer = AccentNormalizer()
    lexer_normalizer = LexerNormalizer()
    sentences = []

    with codecs.open(corpus_path, 'r', 'utf-8') as abstract_sentences:
        for line in tqdm(abstract_sentences.readlines()):
            # normalized
            line = accent_normalizer.normalize(line)
            line = lexer_normalizer.normalize(line)

            if line.strip() != '':
                # remove all leading and trailing punctuation
                sentences.append(filter(lambda x: x != '',
                                        [word.strip('",.=?!\'\*:') for word in re.split(r'\s|;|\[|\]|\(|\)|=',
                                                                                        line.lower())]))
    return sentences


def build_dictionary(corpus_path):
    sentences = corpus_2_sentences(corpus_path)

    with open(DICT_DIR + 'wiki_abstract.dict.pickle', 'w') as dict_file:
        dictionary = FreqDist(sentences)
        dict_file.write(pickle.dumps(dictionary))

    return dictionary


def load_dictionary():
    with open(DICT_DIR + 'wiki_abstract.dict.pickle', 'r') as dict_file:
        return pickle.loads(dict_file.read())


def generate_bigram(sentences):
    bigram_count = Counter()

    for sentence in sentences():
        bigrams = ngrams(sentence, 2)


def train_w2v(sentences):
    model = Word2Vec(sentences, size=100, window=10, min_count=5, workers=NUM_WORKERS)
    model.save("../data/models/wiki_abstract_100_c_5.w2v")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)

    # sentences = corpus_2_sentences('../data/abstract/sentences.copus')
    # joblib.dump(sentences, '../data/abstract/sentences.bin')

    # dictionary = build_dictionary('../data/abstract/sentences.copus')
    # dictionary = load_dictionary()
    #
    # for word,_ in dictionary.most_common(20):
    #     print word

    # for i in xrange(2, 10):
    #     small_dict = filter(lambda x: x[1] > i, dictionary.items())
    #
    #     print "%d: %d" % (i, len(small_dict))
    #     print _heapq.nsmallest(10, small_dict, lambda x: x[1])

    # build bi-gram dictionary
    sentences = joblib.load('../data/abstract/sentences.bin')
    model = Word2Vec.load("../data/models/wiki_abstract_100_c_5.w2v")

    bigrams = []
    for sentence in sentences:
        bigrams.extend(ngrams(sentence, 2))

    bigrams_counter = Counter(bigrams)

    index2word = []
    vectors = []
    for gram, count in bigrams_counter.most_common():
        if gram[0] in model and gram[1] in model:
            index2word.append(gram)
            vectors.append(model[gram[1]] - model[gram[0]])

    joblib.dump(index2word, '../data/models/bigram_index2word', compress=True)
    joblib.dump(vectors, '../data/models/bigram_vectors', compress=True)
